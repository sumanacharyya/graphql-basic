import { useState } from "react";
import { useQuery, gql } from "@apollo/client";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";

const query = gql`
    query GetAllTodosWithUser {
        getTodos {
            id
            title
            userId
            completed
            user {
                id
                username
                name
                email
                phone
            }
        }
    }
`;

function App() {
    const [count, setCount] = useState(0);
    const { data, loading, error } = useQuery(query);

    if (loading) return <h1>Loading...!</h1>;
    if (error) return <h1>Error found...!</h1>;

    console.log(data?.getTodos);

    return (
        <>
            <div>
                <a href="https://vitejs.dev" target="_blank">
                    <img src={viteLogo} className="logo" alt="Vite logo" />
                </a>
                <a href="https://react.dev" target="_blank">
                    <img
                        src={reactLogo}
                        className="logo react"
                        alt="React logo"
                    />
                </a>
            </div>
            <h1>Vite + React</h1>
            <h2>{"Fetched Data!"}</h2>
            <table>
                <tbody>
                    <tr>
                        <th>Todo Title</th>
                        <th>User Id</th>
                        <th>Completed</th>
                        <th>Userid</th>
                        <th>Username</th>
                        <th>User Name</th>
                        <th>User Email</th>
                        <th>User Phone</th>
                    </tr>
                    {Array.isArray(data?.getTodos) &&
                        data?.getTodos.length > 0 &&
                        data?.getTodos.map((eactItem) => (
                            <tr key={eactItem?.id}>
                                <td>{eactItem?.title}</td>
                                <td>{eactItem?.userId}</td>
                                <td>{eactItem?.completed ? "YES" : "NO"}</td>
                                <td>{eactItem?.user?.id}</td>
                                <td>{eactItem?.user?.username}</td>
                                <td>{eactItem?.user?.name}</td>
                                <td>{eactItem?.user?.email}</td>
                                <td>{eactItem?.user?.phone}</td>
                            </tr>
                        ))}
                </tbody>
            </table>
            <div className="card">
                <button onClick={() => setCount((count) => count + 1)}>
                    count is {count}
                </button>
                <p>
                    Edit <code>src/App.jsx</code> and save to test HMR
                </p>
            </div>
            <p className="read-the-docs">
                Click on the Vite and React logos to learn more
            </p>
        </>
    );
}

export default App;
